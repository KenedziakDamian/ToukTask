package com.example.carparkingproject.repository;

import com.example.carparkingproject.model.Parkometer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkometerRepository extends JpaRepository<Parkometer, Long> {
    // Parkometer findParkometerById(Long id);
}
