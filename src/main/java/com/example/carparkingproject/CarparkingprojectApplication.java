package com.example.carparkingproject;

import com.example.carparkingproject.repository.DriverRepository;
import com.example.carparkingproject.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@EnableResourceServer
@CrossOrigin
public class  CarparkingprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarparkingprojectApplication.class, args);
    }

    @Autowired
    public void autenticationManager(AuthenticationManagerBuilder authenticationManagerBuilder, DriverRepository driverRepository) throws Exception {
        authenticationManagerBuilder.userDetailsService(new DriverService(driverRepository));

    }

}