package com.example.carparkingproject.service;

import com.example.carparkingproject.model.Driver;
import com.example.carparkingproject.repository.DriverRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("userDetailsService")
public class DriverService implements UserDetailsService {
    DriverRepository driverRepository;

    public DriverService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public Boolean startParking(String currentUserName) {
        Driver driver = driverRepository.findOneByUsername(currentUserName);
        if (driver.getParkometer().getIsParking() == false) {
            driver.getParkometer().setStartTime(new Date());
            driver.getParkometer().setIsParking(true);
            this.driverRepository.save(driver);
            return true;
        }
        return false;
    }

    public double stopParking(String currentUserName) {
        double toPay = 0;
        Driver driver = driverRepository.findOneByUsername(currentUserName);
        if (driver.getParkometer().getIsParking() == true) {
            toPay = payment(driver);
            driver.getParkometer().setIsParking(false);
            driver.getParkometer().setStartTime(null);
            driver.setAccountBalance(new Double(driver.getAccountBalance() - toPay));
            this.driverRepository.save(driver);
            return toPay;
        }
        return toPay;
    }

    public void driverUpdate(Driver d) {
        driverRepository.save(d);
    }

    public double payment(Driver driver) {
        if (driver.getParkometer().getStartTime() == null)
            return 0;
        Date startDate = driver.getParkometer().getStartTime();
        Date finishDate = new Date();
        double different = finishDate.getTime() - startDate.getTime();
        double hoursinMili = 1000 * 60 * 60;
        double numberOfHours = different / hoursinMili;
        if (driver.getDriverType().equals(Driver.DriverType.VIP)) {
            if (numberOfHours < 1) {
                return 0;
            }
            if (numberOfHours < 2) {
                return 2;
            }
            numberOfHours = numberOfHours - 2;
            double payment = 2;
            double lastHourPayment = 2;
            while (numberOfHours > 0) {
                lastHourPayment = 1.2 * lastHourPayment;
                payment = payment + lastHourPayment;
                numberOfHours--;
            }
        } else {
            if (numberOfHours < 1) {
                return 1;
            }
            if (numberOfHours < 2) {
                return 3;
            }
            numberOfHours = numberOfHours - 2;
            double payment = 3;
            double lastHourPayment = 2;
            while (numberOfHours > 0) {
                lastHourPayment = 1.5 * lastHourPayment;
                payment = payment + lastHourPayment;
                numberOfHours--;
            }
            return payment;

        }

        return 0;
    }

    public Driver getDriver(String username) {
        return driverRepository.findOneByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String s) {
        return driverRepository.findOneByUsername(s);
    }


}

