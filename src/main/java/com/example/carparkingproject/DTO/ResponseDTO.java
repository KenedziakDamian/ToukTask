package com.example.carparkingproject.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;

@Getter
@Setter
public class ResponseDTO {
    Boolean isSuccess = true;
    LinkedList<String> msg = new LinkedList<>();

}
