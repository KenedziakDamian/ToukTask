package com.example.carparkingproject.controller;

import com.example.carparkingproject.DTO.ResponseDTO;
import com.example.carparkingproject.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ParkingControllerAPI {
    @Autowired
    DriverService driverService;
 
    @RequestMapping(value = "/api/start/", method = RequestMethod.GET)
    public ResponseDTO startParking() {
        ResponseDTO responseDTO = new ResponseDTO();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            if (driverService.startParking(currentUserName))
                return responseDTO;
            responseDTO.setIsSuccess(false);
            responseDTO.getMsg().add("Error: cant find usr");

        }
        responseDTO.setIsSuccess(false);
        responseDTO.getMsg().add("Error: system err");
        return responseDTO;
    }


    @RequestMapping(value = "/api/stop/", method = RequestMethod.GET)
    public ResponseDTO stopParking() {
        ResponseDTO responseDTO = new ResponseDTO();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            responseDTO.getMsg().add("Parking finished. Payment = " + driverService.stopParking(currentUserName));
            return responseDTO;
        }
        responseDTO.setIsSuccess(false);
        responseDTO.getMsg().add("Error: system err");
        return responseDTO;

    }


    @RequestMapping(value = "/api/payment/", method = RequestMethod.GET)
    public ResponseDTO parkingPayment() {
        ResponseDTO responseDTO = new ResponseDTO();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            responseDTO.getMsg().add("Payment = " + driverService.payment(driverService.getDriver(currentUserName)));
            return responseDTO;
        }

        return responseDTO;
    }

}
