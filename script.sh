sudo apt-get update
sudo apt-get install maven
sudo apt-get install default-jdk
sudo apt-get install postgresql-9.5
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'haslo';"

sudo -u postgres psql postgres -c "DROP DATABASE \"carParkingDB\""
sudo -u postgres psql postgres -c "CREATE DATABASE \"carParkingDB\"
                WITH OWNER = postgres
                ENCODING = 'UTF8'
                TABLESPACE = pg_default
                CONNECTION LIMIT = -1;"



mvn package
java -jar ./target/*.jar
